##Python-tool:
###	This repository contains source code for performing two tasks:

1. Searching for the ten largest files on the system.
2. Cleaning Desktop by moving files to appropriate folders in the Documents directory based on file extension.

###Prerequisites:

1. Python3

###Execution:

1. Download tool.py.
2. Linux(Ubuntu) : Open terminal in the directory in which it is downloaded and execute the command "python3 tool.py".
3. Windows : Double-click to open.
